//
//  TDLocalizer.m
//  PebbleMonitor
//
//  Created by Tobias Donder on 21.08.14.
//  Copyright (c) 2014 TouDou. All rights reserved.
//

#import "TDLocalizer.h"

@implementation TDLocalizer

NSString * const NotFoundMessage = @">>%@<<";

#pragma mark - Public
+ (instancetype)sharedLocalizer
{
    static TDLocalizer *sharedLocalizer = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedLocalizer = [[self alloc] init];
    });
    
    return sharedLocalizer;
}

- (NSString *)localizedStringForKey:(NSString *)key
{
    return [self localizedStringForKey:key table:nil];
}

- (NSString *)localizedStringForKey:(NSString *)key table:(NSString *)table
{
    if ([key isEqualToString:@""]) {
        return key;
    }
    
    return [self.resourceBundle localizedStringForKey:key
                                                value:[self formatMissingKeyStringForKey:key]
                                                table:table];
}

#pragma mark - Properties
- (NSBundle *)resourceBundle
{
    // If no resource bundle was set, we try to use the MainBundle.
    if (!_resourceBundle) {
        _resourceBundle = [NSBundle mainBundle];
    }
    
    NSAssert(_resourceBundle != nil, @"The resource bundle to get strings from should be the MainBundle or must be set via -[TDLocalizer setRessourceBundle:] before using %s.", __PRETTY_FUNCTION__);
    
    return _resourceBundle;
}

#pragma mark - Private
- (NSString *)formatMissingKeyStringForKey:(NSString *)key
{
    return [NSString stringWithFormat:NotFoundMessage, key];
}

@end
