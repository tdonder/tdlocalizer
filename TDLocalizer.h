//
//  TDLocalizer.h
//  PebbleMonitor
//
//  Created by Tobias Donder on 21.08.14.
//  Copyright (c) 2014 TouDou. All rights reserved.
//

#import <Foundation/Foundation.h>

#define TDLocalizedStringForKey(key) [[TDLocalizer sharedLocalizer] localizedStringForKey:key]

@interface TDLocalizer : NSObject

@property (nonatomic) NSBundle *resourceBundle;

+ (instancetype)sharedLocalizer;

+ (id)new UNAVAILABLE_ATTRIBUTE;
- (id)init UNAVAILABLE_ATTRIBUTE;

/**
 * Returns localized string for given key if available in table.
 *
 * @param NSString The key to search for.
 *
 * @return NSString The localized string. >>key<< if no translation available.
 */
- (NSString *)localizedStringForKey:(NSString *)key;

/**
 * Returns localized string for given key if available in table.
 *
 * @param NSString The key to search for.
 * @param NSString The table to search in.
 *
 * @return NSString The localized string. >>key<< if no translation available.
 */
- (NSString *)localizedStringForKey:(NSString *)key
                              table:(NSString *)table;

@end
